<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"https://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="js/jquery-slim-min.js"></script>

<script
	src="js/bootstrap.js"></script>
<link rel="stylesheet"
	href="css/bootstrap.min.css">
<script
	src="js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>My form</title>
<script src="js/jquery.js"
	type="text/javascript"></script>
<script type="text/javascript">
	function disableFun() {
		$('#editUser').hide();
	}
	function Login() {
		var uname = document.getElementById("username").value;
		var password = document.getElementById("password").value;
		var letters = /^[A-Za-z]+$/;
		var pwd_expression = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/;
		var retVal = true;
		if (!$('#username').val()) {
			$("#username").parent().next(".validation").remove();
			if ($("#username").parent().next(".validation").length == 0) // only add if not added
			{
				$("#username")
						.parent()
						.after(
								"<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Username</div>");
			}
			retVal = false;
		} else {
			if (!letters.test($('#username').val())) {

				$("#username").parent().next(".validation").remove();

				$("#username")
						.parent()
						.after(
								"<div class='validation' style='color:red;margin-bottom: 20px;'>username field requires only alphabets</div>");
				retVal = false;
			} else {
				$("#username").parent().next(".validation").remove();
				retVal = true;
			}
		}
		if (!$('#password').val()) {
			$("#password").parent().next(".validation").remove();
			if ($("#password").parent().next(".validation").length == 0) // only add if not added
			{
				$("#password")
						.parent()
						.after(
								"<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Password</div>");
			}
			retVal = false;
		}

		else {
			$("#password").parent().next(".validation").remove();
			if (retVal)
				retVal = true;
		}
		return retVal;
	}

</script>
<style>
h1 {
	text-align: "center";
	font-size: "20px";
}

body {
	background-color: Skyblue;
}

.astric {
	color: red;
}

.validation {
	color: red;
	margin-bottom: 20px;
}

.result {
	color: black;
	font-family: Arial, Helvetica, sans-serif;
}
</style>
</head>
<body>
	<div align="center">
		<form id="loginForm" onsubmit="return Login()" action="LoginServlet"
			method="post">
			<div class="login">
				<table cellspacing="2" align="center" cellpadding="8" border="0">
					<tr>
						<td align="right">UserName <span class="astric">*</span>
						</td>
						<td><input align="left" type="text"
							placeholder="Enter username" id="username" name="username" /></td>
					</tr>
					<tr>
						<td align="right">Password <span class="astric">*</span>
						</td>
						<td><input type="password" placeholder="Enter password"
							id="password" name="password" class="tb" /></td>
					</tr>
				</table>
			</div>
			<!-- Enter username: <input type="text" id="username" name="username"></br>
			</br> Enter password: <input type="password" id="password" name="password"></br> </br>
			 -->
			<div align="center">
				<button id="submit" type="submit" value="login">Login</button>
				<a href="Register.jsp"> Register</a>

			</div>

		</form>

		<br>
		<div class="result" id="ajaxGetUserServletResponse"></div>



	</div>
</body>

</html>