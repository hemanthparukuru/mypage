 package com.springgradleh2.springgradleh2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springgradleh2Application {

	public static void main(String[] args) {
		SpringApplication.run(Springgradleh2Application.class, args);
	}

}
 