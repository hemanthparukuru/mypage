package com.springgradleh2.springgradleh2.controller;

import java.net.http.HttpResponse;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.springgradleh2.springgradleh2.CustomUserDetailsService;
import com.springgradleh2.springgradleh2.entity.LoginDetails;
import com.springgradleh2.springgradleh2.entity.User;
import com.springgradleh2.springgradleh2.repository.UserRepository;


@RestController
public class AuthController {
	
	@Autowired
    private CustomUserDetailsService userService;
	
	 @RequestMapping(value = "/login", method = RequestMethod.GET)
	    public ModelAndView login() {
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("login");
	        return modelAndView;
	    }
	 
	 @RequestMapping(value = "/signup", method = RequestMethod.GET)
	    public ModelAndView signup() {
	        ModelAndView modelAndView = new ModelAndView();
	        User user = new User();
	        modelAndView.addObject("user", user);
	        modelAndView.setViewName("signup");
	        return modelAndView;
	    }
	 
	 @RequestMapping(value = "/signup", method = RequestMethod.POST)
	    public ModelAndView createNewUser(User user, BindingResult bindingResult) {
	        ModelAndView modelAndView = new ModelAndView();
	        User userExists = userService.findUserByEmail(user.getEmail());
	        if (userExists != null) {
	            bindingResult
	                .rejectValue("email", "error.user",
	                        "There is already a user registered with the username provided");
	        }
	        if (bindingResult.hasErrors()) {
	            modelAndView.setViewName("signup");
	        } else {
	            userService.saveUser(user);
	            modelAndView.addObject("successMessage", "User has been registered successfully");
	            modelAndView.addObject("user", new User());
	            modelAndView.setViewName("login");

	        }
	        return modelAndView;
	    }
	 	
	

}
