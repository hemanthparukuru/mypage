package com.springgradleh2.springgradleh2.controller;

import java.net.http.HttpResponse;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.springgradleh2.springgradleh2.entity.LoginDetails;
import com.springgradleh2.springgradleh2.repository.UserRepository;


@RestController
public class UserController {
	
	@Autowired
	UserRepository userRepository;
	
	@RequestMapping(value = "/hello")
	@ResponseBody  
	    public String hello(){
	        return "Spring boot Gradle Example";
	    }
	
		/*
		 * @RequestMapping("/login.html") public ModelAndView login(Model model) { //
		 * return "login.html"; System.out.print("login----"+model); ModelAndView mav =
		 * new ModelAndView("login"); mav.addObject("hoa_version", "HOA v0.1"); return
		 * mav; }
		 */
	  
	  /*@RequestMapping(value = "/login", method = RequestMethod.GET)
	    public ModelAndView login() {
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("login");
	    modelAndView.addObject("user", new LoginDetails());
	        return modelAndView;
	    }*/
	  
	 /* @RequestMapping( value = "/getUser", method = RequestMethod.POST)
	    public ModelAndView userByNameAndPwd(LoginDetails user, BindingResult bindingResult) {
	        ModelAndView modelAndView = new ModelAndView();*/
			/*
			 * LoginDetails loginDetails =
			 * userRepository.findByUsernameAndPassword(user.getUsername(),
			 * user.getPassword());
			 * //loginDetails.setUsername(userRepository.findByUsername(name);
			 * System.out.println("user list ------"+ user.getUsername() + "::::");
			 * //loginDetails.get(0),HttpStatus.OK);
			 * 
			 * modelAndView.addObject("currentUser", loginDetails);
			 * modelAndView.addObject("fullName", "Welcome " + loginDetails.getUsername());
			 * modelAndView.addObject("adminMessage", loginDetails.getEmailid());
			 * modelAndView.addObject("user",
			 * userRepository.findById(loginDetails.getId()));
			 * modelAndView.setViewName("userDetails");
			 */
	        /*return modelAndView;
	    }*/

	    // Login form with error  
	   /* @RequestMapping("/login-error.html")  
	    public String loginError(Model model) {  
	        model.addAttribute("loginError", true);  
	        return "login.html";  
	    }  */
	
		/*
		 * @RequestMapping(value = "getUser/{username}/{password}") public
		 * ResponseEntity<LoginDetails> getUser(@PathVariable String
		 * username, @PathVariable String password){
		 * System.out.print("username::::"+username+ "::name::::"+password);
		 * userRepository.save(new LoginDetails("Hemanth", "Hemanth@123",
		 * "hemanth@gmail.com","1234512345")); LoginDetails loginDetails =
		 * userRepository.findByUsername(username);
		 * //loginDetails.setUsername(userRepository.findByUsername(name); return new
		 * ResponseEntity<LoginDetails>(loginDetails,HttpStatus.OK); }
		 */
	
	/*@RequestMapping(value = "getUserList")
    public ResponseEntity<LoginDetails> getAllUser(){
		
		List<LoginDetails> loginDetails = null;//userRepository.findAll();
		//loginDetails.setUsername(userRepository.findByUsername(name);
		System.out.println("user list ------");
		return new ResponseEntity<LoginDetails>(loginDetails.get(0),HttpStatus.OK);
    }*/
	
	

}
