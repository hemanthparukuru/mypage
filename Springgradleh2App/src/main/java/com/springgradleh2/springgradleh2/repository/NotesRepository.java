package com.springgradleh2.springgradleh2.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.springgradleh2.springgradleh2.entity.LoginDetails;
import com.springgradleh2.springgradleh2.entity.Notes;

public interface NotesRepository extends CrudRepository<Notes, Long> {

	Notes findByTitle(final String title);
}