package com.springgradleh2.springgradleh2.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.springgradleh2.springgradleh2.entity.LoginDetails;
import com.springgradleh2.springgradleh2.entity.Notes;
import com.springgradleh2.springgradleh2.entity.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {

	  Role findByRole(final String role);
}