package com.springgradleh2.springgradleh2.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.springgradleh2.springgradleh2.entity.LoginDetails;
import com.springgradleh2.springgradleh2.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

	 User findByEmail(final String email);

}