package next.admin.ligin.database;


import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.PreparedStatement;

import next.admin.login.bean.Member;
public class RegisterDa0 {

	public final static String DUPLICATE_USER = "name_UNIQUE";
	public final static String DUPLICATE_PHONENO = "phoneno_UNIQUE";
	public final static String DUPLICATE_EMAILID = "emailid_UNIQUE";
	
	public String insert(Member member, String formType) {
		
		Connection con =  DataBaseConnection.getConnection();
		String result = " Hello "+ member.getUname()+ " registred successfully..:)";
		
		PreparedStatement ps = null;
		try {
			if(formType == null || !formType.equals("edit")) {
			createUser(ps, con, member);
			}else {
				editUser(ps, con, member);
				result = " Hello "+ member.getUname()+ " modified successfully..:)";
			}
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
			if(e.getMessage().contains(DUPLICATE_USER)) {
				result = "Oops user name already exists";
			}
			else if(e.getMessage().contains(DUPLICATE_EMAILID)){
				// TODO Auto-generated catch block
					result = "Oops Email id already exists";
			} else if(e.getMessage().contains(DUPLICATE_PHONENO)) {
				result = "Oops Phone no already exists";
			}
			
			else {
				if(formType == null)
				result = " Oops user "+ member.getUname() + " not registered ..:(";
				else
					result = " Oops user "+ member.getUname() + " not modified, please try again ..:(";
			}
		
	}
		finally
		{
			if(con!=null)
			
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			
		}
		return result;
	}
	
	private void createUser(PreparedStatement ps, Connection con, Member member) throws SQLException{
		String sql = "insert into  database1.login_details (username, password, emailid, phoneno,createdby,modifiedby,createdon,modifiedon) values(?,?,?,?,?,?,?,?)";
		ps = (PreparedStatement) con.prepareStatement(sql);
		long millis=System.currentTimeMillis();
		ps.setString(1, member.getUname());
		ps.setString(2, member.getPassword());
		ps.setString(3, member.getEmailid());
		ps.setString(4, member.getPhoneno());
		ps.setString(5, member.getUname());
		ps.setString(6, member.getUname());
		ps.setDate(7, new Date(millis));
		ps.setDate(8, new Date(millis));
	    ps.executeUpdate();
	    ps.close();
	}
	
	private void editUser(PreparedStatement ps, Connection con, Member member) throws SQLException{
		String sql = "update database1.login_details set password=?, emailid=?, phoneno=?, modifiedon=?, modifiedby=? where username =?";
		long millis=System.currentTimeMillis();
		ps = (PreparedStatement) con.prepareStatement(sql);
		ps.setString(1, member.getPassword());
		ps.setString(2, member.getEmailid());
		ps.setString(3, member.getPhoneno());
		ps.setString(5, member.getUname());
		ps.setDate(4, new Date(millis));
		ps.setString(6, member.getUname());
	    ps.executeUpdate();
	    ps.close();
	}
	
}

