package next.admin.login.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import next.admin.ligin.database.LoginDao;
import next.admin.login.bean.Member;
import next.admin.login.bean.loginbean;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userid = request.getParameter("username");
		String password = request.getParameter("password");
		loginbean bean = new loginbean();
		HttpSession session = request.getSession();
		bean.setUsername(userid);
		bean.setPassword(password);

		LoginDao logindao = new LoginDao();
		Member member = logindao.getUser(bean);
		if (member != null) {
			String greetings = "Hello " + userid + " successfully logged in.";
			response.setContentType("text/html");
			session.setAttribute("username", member.getUname());
			session.setAttribute("emailid", member.getEmailid());
			session.setAttribute("password", member.getPassword());
			session.setAttribute("phoneno", member.getPhoneno());
			RequestDispatcher dispatcher = request.getRequestDispatcher("editUser.jsp");
			dispatcher.forward(request, response);
		} else {
			String greetings = " Oops " + userid + " not a valid user.";
			response.setContentType("text/html");
			response.getWriter()
					.write("<html><head>" + "<meta charset=\"ISO-8859-1\">" + "<title>Validation page</title>"
							+ "</head><body><div align=\"center\">" + greetings
							+ "<br> <a href=\"Login.jsp\"> Go back to login</a> </div></body></html>");
		}

	}

}
