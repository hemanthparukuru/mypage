package next.admin.login.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import next.admin.ligin.database.RegisterDa0;
import next.admin.login.bean.Member;

/**
 * Servlet implementation class Register
 */
@WebServlet("/CreateModifyServlet")
public class CreateModifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateModifyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		String uname = request.getParameter("username");  
		String password = request.getParameter("password");
		String emailid = request.getParameter("emailid");  
		String phoneno = request.getParameter("phoneno");
		String editForm = request.getParameter("editForm");
		Member member=new Member(uname, password, emailid, phoneno);
		
		RegisterDa0 rdao = new RegisterDa0();
		String result = rdao.insert(member, editForm); 
		if(result != null)
		{
			String greetings1 = result;
			response.setContentType("text/plain");
			response.getWriter().write(greetings1);
		}
		else
		{
			String greetings =  result;
			response.setContentType("text/plain");
			response.getWriter().write(greetings);
		}
		
		
		//response.sendRedirect("Register2.jsp");
	}

}