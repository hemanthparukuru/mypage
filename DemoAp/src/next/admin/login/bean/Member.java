package next.admin.login.bean;


public class Member {
	private String uname,password,emailid,phoneno;
	public Member() {
		super();
	}
	public Member(String uname, String password, String emailid, String phoneno) {
		this.uname = uname;
		this.password = password;
		this.emailid = emailid;
		this.phoneno = phoneno;
	}
	public Member(String uname, String password, String emailid, String phoneno, String address) {
		super();
		this.uname = uname;
		this.password = password;
		this.emailid = emailid;
		this.phoneno = phoneno;
	}
	public  String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
}

