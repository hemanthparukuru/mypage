<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>

<script
	src="js/bootstrap.js"></script>
	<script
	src="js/app-ajax.js"></script>
<link rel="stylesheet"
	href="css/bootstrap.min.css">
<script
	src="js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>My form</title>
<script src="js/jquery.js"
	type="text/javascript"></script>
	<script type="text/javascript">
function disableFun() {
	$('#loginPage').hide();
	
}

function regCall()
	{
	var retVal = validate();
		
	 
			if(retVal)
				{
				$('#ajaxGetUserServletResponse').val("");
				$.ajax({
					type:"post",
					url : 'CreateModifyServlet',
					data : {
						username : $('#username').val(),
						password : $('#password').val(),
						emailid  : $('#emailid').val(),
						phoneno  : $('#phoneno').val(),
						editForm : "edit"
					},
					success : function(responseText) {
						$('#ajaxGetUserServletResponse').text(responseText);
						if(responseText.indexOf("Oops") == -1) {
							$('#editform').hide();
							$('#loginPage').show();
						}
					},
					error : function(error) {
						$('#ajaxGetUserServletResponse').text(error);
					}
				});
				}
		}
	
	</script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>My page</title>
				<script src="https://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>


<style>
			body
			{
				background-color:Skyblue;
			}
			.astric {
				color: red;
			}
			.validation
    		{
     			 color: red;
     			 margin-bottom: 20px;
   			 } 
	</style>
	<nav class="navbar navbar-dark bg-dark">
  		<h3 style="color:red">this the single page application.</h3>
</nav>
</head>
<body onload="disableFun()">
	<div align="center">
		<form id = "editform">
			<!-- Main div code -->
			<div id="main">
				<div class="h-tag">
					<h2> <%= session.getAttribute("username") %> You have successfully logged in. </h2><br>
					<h3>If u want to modify your account you can Modify Your Account here</h3>
				</div>
				<!-- create account div -->
				<div class="login">
					<table cellspacing="2"   align="center" cellpadding="8" border="0">
						<tr>
							<td align="right"> UserName </td>
							<td><input  align="left" type="text" readonly value = <%=session.getAttribute("username") %>
								id="username" name="username" /></td> 
						</tr>
						<tr>
							<td align="right"> Password <span class="astric">*</span> </td>
							<td><input type="password" value = <%=session.getAttribute("password") %>
								id="password" name="password" class="tb" /></td>
						</tr>
						<tr>
							<td align="right">Cnfrm Password <span class="astric">*</span> </td>
							<td><input type="password" value = <%=session.getAttribute("password") %>
								id="cnfrmpassword" name="cnfrmpassword" class="tb" /></td>
						</tr>
						<tr>
							<td align="right">Email id  <span class="astric">*</span></td>
							<td><input type="text" value = <%=session.getAttribute("emailid") %>
								id="emailid" name="emailid" class="tb" /></td>
						</tr>
						<tr>
							<td align="right">Phone no <span class="astric">*</span> </td>
							<td><input type="text" value = <%=session.getAttribute("phoneno") %>
								id="phoneno" name="phoneno" class="tb" /></td>
						</tr>
						<tr> <td/><td><span class="astric">*</span> Denotes required fields</td></tr>
						<tr>
							<td><a href="Login.jsp">   Go back to login</a></td>
							<td>
								<input type="button" id="submit" value="Modify" class="btn"
								 onClick="regCall()"/>
								 </td>
						</tr>
						
					</table>
				</div>
				<!-- create account box ending here.. -->
			</div>
			<!-- Main div ending here... -->
			<br>
		</form>
			<div class="result" id="ajaxGetUserServletResponse"></div>
			<br>
			<a id="loginPage" href="Login.jsp">   Go back to login</a>
			</div>
</body>
</html>

