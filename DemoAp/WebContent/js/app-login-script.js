
$(document).ready(function() {
	$('#submit').click(function() {
		$.ajax({
			url : 'LoginServlet',
			data : {
				userName : $('#userid').val(),
				password : $('#password').val()
			},
			success : function(responseText) {
				$('#ajaxGetUserServletResponse').text(responseText);
			},
			error : function(error) {
				$('#ajaxGetUserServletResponse').text(error);
			}
		});
	});
});

