
function validate()
	{
		var uname= document.getElementById("username").value;
		var password= document.getElementById("password").value;
		var cnfrmpassword= document.getElementById("cnfrmpassword").value;
		var emailid= document.getElementById("emailid").value;
		var phoneno= document.getElementById("phoneno").value;
		var retVal = true;
		
		var pwd_expression = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/;
		var letters = /^[A-Za-z]+$/;
		var digits = /^[0-9]+$/;
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!$('#username').val()) {
			$("#username").parent().next(".validation").remove();
			 if ($("#username").parent().next(".validation").length == 0) // only add if not added
		        {
		            $("#username").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Username</div>");
		        }
			 retVal = false;
        }
		 else {
			 if(!letters.test($('#username').val())){
				$("#username").parent().next(".validation").remove();
				 
				 $("#username").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>username field requires only alphabets</div>");
			     retVal = false;  
			 }else{
		        $("#username").parent().next(".validation").remove();
		        retVal = true;
			 }
		 }
		if (!$('#password').val()) {
			$("#password").parent().next(".validation").remove();
			 if ($("#password").parent().next(".validation").length == 0) // only add if not added
		        {
		            $("#password").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Password</div>");
		        }
			 retVal = false;
       }
		else {
			 if(!pwd_expression.test($('#password').val())){
				
				 	$("#password").parent().next(".validation").remove();
				 
				 $("#password").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Upper case, Lower case, Special character and Numeric letter are required in Password filed</div>");
			     retVal = false;  
			 }
		 else {
		        $("#password").parent().next(".validation").remove();
		        if(retVal)
		        retVal = true;
		 }
		}
		if (!$('#cnfrmpassword').val()) {
			$("#cnfrmpassword").parent().next(".validation").remove();
			 if ($("#cnfrmpassword").parent().next(".validation").length == 0) // only add if not added
		        {
		            $("#cnfrmpassword").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter cnfrmPassword</div>");
		        }
			 retVal = false;
      }
		
		if($('#cnfrmpassword').val() && $('#password').val()){
			if(($('#cnfrmpassword').val() !== $('#password').val())){
				$("#cnfrmpassword").parent().next(".validation").remove();
				$("#cnfrmpassword").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>password and  cnfrm password should be same</div>");
				retVal = false;
			}else {
				$("#cnfrmpassword").parent().next(".validation").remove();
				if(retVal)
				retVal = true;
			}
		}  
		
			
		if (!$('#emailid').val()) {
			$("#emailid").parent().next(".validation").remove();
			 if ($("#emailid").parent().next(".validation").length == 0) // only add if not added
		        {
		            $("#emailid").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter email id</div>");
		        }
			 retVal = false;
       }
		else if(!filter.test($('#emailid').val())){
			
		 	$("#emailid").parent().next(".validation").remove();
		 
		 $("#emailid").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>enter a valid emailid</div>");
	     retVal = false;  
	 }
		 else {
       		 $("#emailid").parent().next(".validation").remove();
       		 if(retVal)
        		retVal = true;
 }
		
		if (!$('#phoneno').val()) {
			$("#phoneno").parent().next(".validation").remove();
			 if ($("#phoneno").parent().next(".validation").length == 0) // only add if not added
		        {
		            $("#phoneno").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter phoneno</div>");
		        }
			 retVal = false;
       }else if(!digits.test($('#phoneno').val())){
			
		 	$("#phoneno").parent().next(".validation").remove();
		 
		 $("#phoneno").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'> Phone no should be in digits</div>");
	     retVal = false;  
	 } else if (digits.test($('#phoneno').val()) && $('#phoneno').val().length != 10){
		 
		$("#phoneno").parent().next(".validation").remove();
		 
		 $("#phoneno").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'> Phone no should be 10 digits</div>");
	     retVal = false;  
		 }

	 
else {
   $("#phoneno").parent().next(".validation").remove();
   if(retVal)
   retVal = true;
}
return retVal;
}