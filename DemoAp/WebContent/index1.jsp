<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"https://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jQuery, Ajax and Servlet/JSP integration example</title>

<script src="https://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function() {
	$('#password').blur(function() {
		$.ajax({
			url : 'LoginServlet',
			data : {
				username : $('#userid').val(),
				password : $('#password').val()
			},
			success : function(responseText) {
				$('#ajaxGetUserServletResponse').text(responseText);
			},
			error : function(error) {
				$('#ajaxGetUserServletResponse').text(error);
			}
		});
	});
});
</script>
</head>
<body>

	<form>
		Enter Your Name: <input type="text" id="userid" />
		Enter Password: <input type="password" id="password" />
		<div align="center">
				<input type="submit" id="submit" value="login"> <a href="Register1.jsp">
					Register</a>
			</div>
	</form>
	<br>
	<br>

	<strong>Ajax Response</strong>:
	<div id="ajaxGetUserServletResponse"></div>
</body>
</html>