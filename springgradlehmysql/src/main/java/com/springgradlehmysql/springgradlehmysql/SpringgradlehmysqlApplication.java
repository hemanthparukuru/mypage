package com.springgradlehmysql.springgradlehmysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringgradlehmysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringgradlehmysqlApplication.class, args);
	}

}
